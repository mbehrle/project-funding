---
title: "Projects"
date: 2021-10-16T15:23:01-04:00
description: "Projects which have been proposed, accepted, or completed under this initiative."
---

## Proposed

| Project | Status |
| :--- | :--- |

## Accepted

| Project | Status |
| :--- | :--- |
| [Gradle 6.4 Packaging](https://salsa.debian.org/freexian-team/project-funding/-/issues/11) | Accepting bids |

## Completed

| Project | Status |
| :--- | :--- |
| [Better no-dsa support in PTS](https://salsa.debian.org/freexian-team/project-funding/-/blob/master/completed/2020-12-pts-no-dsa.md) | Completed |

