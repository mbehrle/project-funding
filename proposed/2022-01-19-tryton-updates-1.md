# Project Data

| Title | Tryton updates for Debian 1 |
| ----- | ----- |
| **Funding source** | LTS or Freexian |
| **Submitter** | Mathias Behrle <mathiasb@m9s.biz>  |
| **Executor** |  Mathias Behrle |
| **Reviewer** | TBD |
| **Proposal date** | 2022-01-19 |
| **Approval date** | TBD |
| **Request for bids** | (insert link to eventual request) |
| **Project management issue** | (insert link when project starts) |
| **Completion date** | (insert date when completed) |

----------------------------------------

# Rationale

The goal of this project is to enable the user to deploy a production ready
Tryton installation out-of-the-box.

# Description

This project should basically solve #998319.
Until now the Tryton Server uses by default the werkzeug development server 
as backend. To run an enterprise grade and scalable setup it is recommended
to run the server on a dedicated wsgi backend served by a reverse proxy.
The systemd start script misses some hardening which could be improved, too.

## Project Details

Please see submitter bid section below;

## Completion Criteria

* 1 Package tryton-server-uwsgi providing a working setup using the uwsgi backend.
* 2 Package tryton-server-nginx provding a working setup using the above backend.
* 3 Tested systemd hardening
* 4 Optional: Package tryton-server-all-in-one provding the general working setup
  using the packages above, depending on tryton-modules-all and providing a
	debconf guided setup including the database setup (dbconfig-common).

## Estimated Effort

Estimation is difficult without going into the realization itself. Therefore I want
to propose to get 3000€ for 5 days (40 hours) of work. As part of this, I guarantee
that I will complete the completion criteria 1-3, as far as possible with criterion 4.
If there is time left I will use it on the project Tryton updates for Debian 2.

# Submitter Bid

- Productivity improvements
  - systemd hardening
  - Packaging and integration of an automatic production grade setup with
    reverse proxy and wsgi backend (could alternatively go into documentation)
    providing several packages (#998319)
  - Full guided setup with debconf and evtl. dbconfig-common 
